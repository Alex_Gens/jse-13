package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.IUserEndpoint;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.User;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.userService = serviceLocator.getUserService();
    }

    @NotNull
    private final IUserService userService;

    @Override
    @WebMethod
    public void mergeUser(
            @Nullable final String token,
            @Nullable final User entity
    ) throws Exception {
        validate(token);
        userService.merge(entity);
    }

    @Override
    @WebMethod
    public void persistUser(@Nullable final User entity) throws Exception {
        userService.persist(entity);
    }

    @Override
    @WebMethod
    public void removeUser(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        userService.remove(id);
    }

    @Override
    @WebMethod
    public void removeAllUsers(@Nullable final String token) throws Exception {
        validate(token);
        userService.removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public User findOneUser(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        return userService.findOne(id);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLogin(
            @Nullable final String token,
            @Nullable final String login
    ) throws Exception {
        validate(token);
        return userService.findByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User findCurrentUser(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return userService.findCurrentUser(currentId);
    }

    @Nullable
    @Override
    @WebMethod
    public List<User> findAllUsers(@Nullable final String token) throws Exception {
        validate(token);
        return userService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllUsersLogins(@Nullable final String token) throws Exception {
        validate(token);
        return userService.findAllUsersLogins();
    }

    @Override
    @WebMethod
    public boolean containsUser(@Nullable final String login) throws Exception {
        return userService.contains(login);
    }

}
