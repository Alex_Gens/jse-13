package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.ITaskEndpoint;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

    @Override
    @WebMethod
    public void mergeTask(
            @Nullable final String token,
            @Nullable final Task entity
    ) throws Exception {
        validate(token);
        taskService.merge(entity);
    }

    @Override
    @WebMethod
    public void persistTask(
            @Nullable final String token,
            @Nullable final Task entity
    ) throws Exception {
        validate(token);
        taskService.persist(entity);
    }

    @Override
    @WebMethod
    public void createTask(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        taskService.create(currentId, name);
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        taskService.remove(id);
    }

    @Override
    @WebMethod
    public void removeTasksWithProject(
            @Nullable final String token,
            @Nullable final String projectId
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        taskService.removeWithProject(currentId, projectId);
    }

    @Override
    @WebMethod
    public void removeAllTasks(@Nullable final String token) throws Exception {
        validate(token);
        taskService.removeAll();
    }

    @Override
    @WebMethod
    public void removeAllTasksByCurrentId(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        taskService.removeAll(currentId);
    }

    @Override
    @WebMethod
    public void removeAllTasksWithProjects(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        taskService.removeAllWithProjects(currentId);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findByTaskNameCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findByName(currentId, name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findOneTask(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        return taskService.findOne(id);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findAllTasks(@Nullable final String token) throws Exception {
        validate(token);
        return taskService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllSortedTasksByCurrentIdProjectId(
            @Nullable final String token,
            @Nullable final String projectId,
            @Nullable final SortType sortType
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findAll(currentId, projectId, sortType);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findAllTasksByCurrentId(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findAll(currentId);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllSortedTasksByCurrentId(
            @Nullable final String token,
            @Nullable final SortType sortType
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findAll(currentId, sortType);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllTasksByNameCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findAllByName(currentId, part);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllTasksByInfoCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findAllByInfo(currentId, part);
    }

    @Override
    @WebMethod
    public boolean containsTask(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        validate(token);
        return taskService.contains(name);
    }

    @Override
    @WebMethod
    public boolean containsTaskByCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.contains(currentId, name);
    }

    @Override
    @WebMethod
    public boolean isEmptyTaskRepositoryByCurrentId(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.isEmptyRepository(currentId);
    }

}
