package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.IDomainEndpoint;
import ru.kazakov.iteco.api.service.IDomainService;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    private final IDomainService domainService;

    public DomainEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.domainService = serviceLocator.getDomainService();
    }

    @Override
    @WebMethod
    public void saveDomainBin(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        validate(token);
        domainService.saveBin(directory, fileName);
    }

    @Override
    @WebMethod
    public void saveDomainJaxbXml(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        validate(token);
        domainService.saveJaxbXml(directory, fileName);
    }

    @Override
    @WebMethod
    public void saveDomainJaxbJson(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        validate(token);
        domainService.saveJaxbJson(directory, fileName);
    }

    @Override
    @WebMethod
    public void saveDomainFasterXml(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        validate(token);
        domainService.saveFasterXml(directory, fileName);
    }

    @Override
    @WebMethod
    public void saveDomainFasterJson(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        validate(token);
        domainService.saveFasterJson(directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainBin(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        domainService.loadBin(currentId, directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainJaxbXml(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        domainService.loadJaxbXml(currentId, directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainJaxbJson(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        domainService.loadJaxbJson(currentId, directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainFasterXml(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        domainService.loadFasterXml(currentId, directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainFasterJson(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        domainService.loadFasterJson(currentId, directory, fileName);
    }

    @Override
    @WebMethod
    public boolean isDomainDirectory(
            @Nullable final String token,
            @Nullable final String directory
    ) throws Exception {
        validate(token);
        return domainService.isDirectory(directory);
    }

    @Override
    @WebMethod
    public boolean existDomain(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        validate(token);
        return domainService.exist(directory, fileName);
    }

}
