package ru.kazakov.iteco.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.Status;
import java.util.Date;

@Data
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @NotNull
    private String userId;

    @Nullable
    private String projectId;

    @Nullable
    private String name;

    @Nullable
    private Date dateCreate = new Date();

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @Nullable
    private String info;

    public Task(@NotNull final String userId) {this.userId = userId;}

    public boolean isEmpty() {return info == null || info.isEmpty();}

}
