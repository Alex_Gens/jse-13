package ru.kazakov.iteco.entity;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property= "ru/kazakov/iteco/entity")
@JsonSubTypes({
        @JsonSubTypes.Type(value=Project.class, name="project"),
        @JsonSubTypes.Type(value=Task.class, name="task"),
        @JsonSubTypes.Type(value=User.class, name="user")})
public abstract class AbstractEntity implements Serializable {

    public static final long serialVersionUID = 1L;

    @NotNull
    protected String id = UUID.randomUUID().toString();

}
