package ru.kazakov.iteco.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.RoleType;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity {

    @NotNull
    private String userId;

    private long timestamp = System.currentTimeMillis();

    @NotNull
    private RoleType roleType;

    @Nullable
    private  String signature;

}
