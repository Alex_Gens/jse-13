package ru.kazakov.iteco.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.enumeration.Status;
import ru.kazakov.iteco.util.AES;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import static ru.kazakov.iteco.constant.Constant.*;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {this.serviceLocator = serviceLocator;}

    @Override
    public void persist(
            @Nullable final Project entity
    ) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            repository.persistProject(entity);
            session.commit();
        } catch (SQLException e) {session.rollback();}
          finally {session.close();}
    }

    @Override
    public void merge(@Nullable final Project entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            repository.mergeProject(entity);
            session.commit();
        } catch (SQLException e) {session.rollback();}
          finally {session.close();}
    }

    @Override
    public void create(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        @NotNull final String secret = serviceLocator.getPropertyService().getSecret();
        @NotNull final String decryptToken = AES.decrypt(token, secret);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @Nullable final Session session = mapper.readValue(decryptToken, Session.class);
        if (session == null) throw new Exception();
        @NotNull final Project project = new Project();
        project.setUserId(session.getUserId());
        project.setName(name);
        persist(project);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            repository.removeProject(id);
            session.commit();
        } catch (SQLException e) {session.rollback();}
          finally {session.close();}
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            repository.removeAllProjects();
            session.commit();
        } catch (SQLException e) {session.rollback();}
          finally {session.close();}
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            repository.removeAllProjectsByCurrentId(currentUserId);
            session.commit();
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        @Nullable final Project project;
        try {
            project = repository.findOneProject(id);
            session.commit();
            return project;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return null;
    }

    @Nullable
    @Override
    public Project findByName(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            @Nullable final Project project =
                    repository.findByProjectNameCurrentId(currentUserId, name);
            session.commit();
            return project;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return null;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            @Nullable final List<Project> projects = repository.findAllProjects();
            session.commit();
            return projects;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            @NotNull final List<Project> projects =
                    repository.findAllProjectsByCurrentId(currentUserId);
            session.commit();
            return projects;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final SortType sortType
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        @NotNull final String orderBy;
        switch (sortType) {
            case START   : orderBy = DATE_START + ", " + DATE_CREATE; break;
            case FINISH  : orderBy = DATE_FINISH + ", " + DATE_CREATE; break;
            case STATUS  : orderBy = String.format("field(status, '%s','%s','%s')",
                           String.valueOf(Status.PLANNED),
                           String.valueOf(Status.IN_PROGRESS),
                           String.valueOf(Status.READY)) + ", " + DATE_CREATE; break;
            default      : orderBy = DATE_CREATE;
        }
        try {
            List<String> projectsNames =
                    repository.findAllSortedProjectsByCurrentId(currentUserId, orderBy);
            session.commit();
            return projectsNames;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public List<String> findAllByName(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            @NotNull final List<String> projectsNames =
                    repository.findAllProjectsByNameCurrentId(currentUserId, "%" + part + "%");
            session.commit();
            return projectsNames;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            @NotNull final List<String> projectsNames =
                    repository.findAllProjectsByInfoCurrentId(currentUserId,"%" + part + "%");
            session.commit();
            return projectsNames;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return Collections.emptyList();
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            final boolean contains = repository.containsProjectByName(name) > 0;
            session.commit();
            return contains;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return false;
    }

    @Override
    public boolean contains(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository
                = session.getMapper(IProjectRepository.class);
        try {
            final boolean contains =
                    repository.containsProjectByNameCurrentId(currentUserId, name) > 0;
            session.commit();
            return contains;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return false;
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            final boolean isEmpty =
                    repository.isEmptyProjectRepository(currentUserId) > 0;
            session.commit();
            return isEmpty;
        } catch (Exception e) {session.rollback();}
          finally {session.close();}
        return true;
    }

}
