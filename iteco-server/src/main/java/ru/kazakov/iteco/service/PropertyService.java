package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.api.service.IPropertyService;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final Properties configProperties = new Properties();

    @NotNull
    private final Properties databaseProperties = new Properties();

    public PropertyService() {
        try(
            InputStream configStream =  PropertyService.class.getClassLoader()
            .getResourceAsStream("config.properties");
            InputStream databaseStream =  PropertyService.class.getClassLoader()
            .getResourceAsStream("database.properties")
        ) {
            configProperties.load(configStream);
            databaseProperties.load(databaseStream);
        } catch (IOException e) {e.getMessage();}
    }

    @NotNull
    @Override
    public String getHost() {return configProperties.getProperty("host");}

    @NotNull
    @Override
    public String getPort() {return configProperties.getProperty("port");}

    @NotNull
    @Override
    public String getSalt() {return configProperties.getProperty("salt");}

    @NotNull
    @Override
    public String getCycle() {return configProperties.getProperty("cycle");}

    @NotNull
    @Override
    public String getSessionTime() {return configProperties.getProperty("sessionTime");}

    @NotNull
    @Override
    public String getSecret() {return configProperties.getProperty("secret");}

    @NotNull
    @Override
    public String getDataBaseDriver() {return databaseProperties.getProperty("driver");}

    @NotNull
    @Override
    public String getDataBaseUrl() {return databaseProperties.getProperty("url");}

    @NotNull
    @Override
    public String getDataBaseUser() {return databaseProperties.getProperty("user");}

    @NotNull
    @Override
    public String getDataBasePassword() {return databaseProperties.getProperty("password");}

}
