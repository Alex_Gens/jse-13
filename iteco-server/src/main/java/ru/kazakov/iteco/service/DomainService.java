package ru.kazakov.iteco.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.RequiredArgsConstructor;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.service.*;
import ru.kazakov.iteco.entity.Domain;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.entity.User;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RequiredArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ISessionService sessionService;

    public void saveBin(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        try (@NotNull final ObjectOutput stream
                     = new ObjectOutputStream(new FileOutputStream(path.toFile()))) {
            @NotNull final Domain domain = getInstance();
            stream.writeObject(domain);
            stream.flush();
            stream.close();
        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public void saveJaxbXml(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final Domain domain = getInstance();
        marshaller.marshal(domain, path.toFile());
    }

    @Override
    public void saveJaxbJson(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        System.setProperty("javax.xml.bind.context.factory",
                "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final Domain domain = getInstance();
        marshaller.marshal(domain, path.toFile());
    }

    @Override
    public void saveFasterXml(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        @NotNull final Domain domain = getInstance();
        mapper.writeValue(path.toFile(), domain);
    }

    @Override
    public void saveFasterJson(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        @NotNull final Domain domain = getInstance();
        mapper.writeValue(path.toFile(), domain);
    }

    @Override
    public void loadBin(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        try (ObjectInputStream stream
                     = new ObjectInputStream(new FileInputStream(path.toFile()))) {
            @NotNull final Domain domain = (Domain) stream.readObject();
            stream.close();
            sessionService.remove(userSessionId);
            load(domain);
        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public void loadJaxbXml(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(path.toFile());
        sessionService.remove(userSessionId);
        load(domain);
    }

    @Override
    public void loadJaxbJson(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        System.setProperty("javax.xml.bind.context.factory",
                "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(path.toFile());
        sessionService.remove(userSessionId);
        load(domain);
    }

    @Override
    public void loadFasterXml(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(path.toFile(), Domain.class);
        sessionService.remove(userSessionId);
        load(domain);
    }

    @Override
    public void loadFasterJson(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(path.toFile(), Domain.class);
        sessionService.remove(userSessionId);
        load(domain);
    }

    @Override
    public boolean isDirectory(@Nullable final String directory) throws Exception {
        if (directory == null || directory.isEmpty()) throw new Exception();
        return Files.isDirectory(Paths.get(directory));
    }

    @Override
    public boolean exist(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if  (directory == null || directory.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        return Files.exists(Paths.get(directory + File.separator + fileName));
    }

    @NotNull
    private Domain getInstance() throws Exception {
        @NotNull final Domain domain = new Domain();
        @Nullable final List<Project> projects = projectService.findAll();
        @Nullable final List<Task> tasks = taskService.findAll();
        @Nullable final List<User> users = userService.findAll();
        if (projects == null) throw new Exception();
        if (tasks == null) throw new Exception();
        if (users == null) throw new Exception();
        domain.setProjects(projects);
        domain.setTasks(tasks);
        domain.setUsers(users);
        return domain;
    }

    private void load(@Nullable final Domain domain) throws Exception {
        if (domain == null) throw new Exception();
        @Nullable final List<Project> projects = domain.getProjects();
        @Nullable final List<Task> tasks = domain.getTasks();
        @Nullable final List<User> users = domain.getUsers();
        if (projects == null) throw new Exception();
        if (tasks == null) throw new Exception();
        if (users == null) throw new Exception();
        projectService.removeAll();
        taskService.removeAll();
        userService.removeAll();
        for (Project project : projects) {projectService.persist(project);}
        for (Task task : tasks) {taskService.persist(task);}
        for (User user : users) {userService.persist(user);}
    }

}
