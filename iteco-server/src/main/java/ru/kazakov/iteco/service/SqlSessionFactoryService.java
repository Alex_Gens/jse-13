package ru.kazakov.iteco.service;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import java.io.IOException;
import java.io.Reader;

public class SqlSessionFactoryService {

    public SqlSessionFactory getSqlSessionFactory() {
        try(Reader reader = Resources.getResourceAsReader("mybatis-config.xml")) {
            return new SqlSessionFactoryBuilder().build(reader);
        } catch (IOException e) {e.printStackTrace(); return null;}
    }

}
