package ru.kazakov.iteco.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.User;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public UserService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void persist(@Nullable final User entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             repository.persistUser(entity);
             session.commit();
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
    }

    @Override
    public void merge(@Nullable final User entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             repository.mergeUser(entity);
             session.commit();
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             repository.removeUser(id);
             session.commit();
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             repository.removeAllUsers();
             session.commit();
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             @Nullable final User user = repository.findOneUser(id);
             session.commit();
             return user;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             @Nullable final User user = repository.findUserByLogin(login);
             session.commit();
             return user;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return null;
    }

    @Nullable
    @Override
    public User findCurrentUser(@Nullable final String currentId) throws Exception {
        if (currentId == null || currentId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             @Nullable final User user = repository.findOneUser(currentId);
             session.commit();
             return user;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return null;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             @Nullable final List<User> users = repository.findAllUsers();
             session.commit();
             return users;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public List<String> findAllUsersLogins() throws Exception {
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             @Nullable final List<String> usersLogins = repository.findAllUsersLogins();
             session.commit();
             return usersLogins;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return Collections.emptyList();
    }

    @Override
    public boolean contains(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final IUserRepository repository
                = session.getMapper(IUserRepository.class);
        try {
             @Nullable final boolean contains = repository.containsUserByLogin(login) > 0;
             session.commit();
             return contains;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return false;
    }

}
