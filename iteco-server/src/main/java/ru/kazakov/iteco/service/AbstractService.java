package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.service.IService;
import java.util.List;

public abstract class AbstractService<T> implements IService<T> {

    @Override
    public abstract void persist(@Nullable final T entity) throws Exception;

    @Override
    public abstract void remove(@Nullable final String id) throws Exception;

    @Override
    public abstract void removeAll() throws Exception;

    @Nullable
    @Override
    public abstract T findOne(@Nullable final String id) throws Exception;

    @NotNull
    @Override
    public abstract List<T> findAll() throws Exception;

}
