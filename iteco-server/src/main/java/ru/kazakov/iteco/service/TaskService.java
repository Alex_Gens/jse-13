package ru.kazakov.iteco.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.enumeration.Status;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import static ru.kazakov.iteco.constant.Constant.*;
import static ru.kazakov.iteco.constant.Constant.DATE_CREATE;

public class TaskService extends AbstractService<Task> implements ITaskService {
    
    @NotNull
    private final ServiceLocator serviceLocator;

    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void persist(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
                repository.persistTask(entity);
                session.commit();
                } catch (SQLException e) {session.rollback();}
                  finally {session.close();}
    }

    @Override
    public void merge(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
                repository.mergeTask(entity);
                session.commit();
                } catch (SQLException e) {session.rollback();}
                  finally {session.close();}
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
                repository.removeTask(id);
                session.commit();
                } catch (SQLException e) {session.rollback();}
                  finally {session.close();}
    }

    @Override
    public void create(
            @Nullable final String currentId,
            @Nullable final String name
    ) throws Exception {
        if (currentId == null || currentId.isEmpty()) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        @NotNull final Task task = new Task();
        task.setUserId(currentId);
        task.setName(name);
        persist(task);
    }

    @Override
    public void removeWithProject(
            @Nullable final String currentUserId,
            @Nullable final String projectId
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
                repository.removeTasksWithProject(currentUserId, projectId);
                session.commit();
                } catch (SQLException e) {session.rollback();}
                  finally {session.close();}
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
             repository.removeAllTasks();
             session.commit();
        } catch (SQLException e) {session.rollback();}
          finally {session.close();}
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
             repository.removeAllTasksByCurrentId(currentUserId);
             session.commit();
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
    }

    @Override
    public void removeAllWithProjects(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
             repository.removeAllTasksWithProjects(currentUserId);
             session.commit();
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
             @Nullable final Task task = repository.findOneTask(id);
             session.commit();
             return task;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return null;
    }

    @Nullable
    @Override
    public Task findByName(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
             @Nullable final Task task =
                     repository.findTaskByNameCurrentId(currentUserId, name);
             session.commit();
             return task;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return null;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
             @Nullable final List<Task> tasks = repository.findAllTasks();
             session.commit();
             return tasks;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        try {
             @Nullable final List<Task> tasks
                     = repository.findAllTasksByCurrentId(currentUserId);
             session.commit();
             return tasks;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final SortType sortType
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        @NotNull final String orderBy;
        switch (sortType) {
            case START   : orderBy = DATE_START + ", " + DATE_CREATE; break;
            case FINISH  : orderBy = DATE_FINISH + ", " + DATE_CREATE; break;
            case STATUS  : orderBy = String.format("field(status, '%s','%s','%s')",
                           String.valueOf(Status.PLANNED),
                           String.valueOf(Status.IN_PROGRESS),
                           String.valueOf(Status.READY)) + ", " + DATE_CREATE; break;
            default      : orderBy = DATE_CREATE;
        }
        try {
             @Nullable final List<String> tasksNames =
                     repository.findAllSortedTasksByCurrentId(currentUserId, orderBy);
             session.commit();
             return tasksNames;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final String projectId,
            @Nullable final SortType sortType
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository
                = session.getMapper(ITaskRepository.class);
        @NotNull final String orderBy;
        switch (sortType) {
            case START   : orderBy = DATE_START + ", " + DATE_CREATE; break;
            case FINISH  : orderBy = DATE_FINISH + ", " + DATE_CREATE; break;
            case STATUS  : orderBy = String.format("field(status, '%s','%s','%s')",
                           String.valueOf(Status.PLANNED),
                           String.valueOf(Status.IN_PROGRESS),
                           String.valueOf(Status.READY)) + ", " + DATE_CREATE; break;
            default      : orderBy = DATE_CREATE;
        }
        try {
             @Nullable final List<String> tasksNames =
                     repository.findAllSortedTasksByCurrentIdProjectId(currentUserId, projectId, orderBy);
             session.commit();
             return tasksNames;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return Collections.emptyList();    }

    @NotNull
    @Override
    public List<String> findAllByName(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
             @Nullable final List<String> tasksNames =
                     repository.findAllTasksByNameCurrentId(currentUserId, "%" + part + "%");
             session.commit();
             return tasksNames;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return Collections.emptyList();
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
             @Nullable final List<String> tasksNames =
                     repository.findAllTasksByInfoCurrentId(currentUserId, "%" + part + "%");
             session.commit();
             return tasksNames;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return Collections.emptyList();
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
             @Nullable final boolean contains = repository.containsTaskByName(name) > 0;
             session.commit();
             return contains;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return false;
    }

    @Override
    public boolean contains(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
             @Nullable final boolean contains =
                     repository.containsTaskByNameCurrentId(currentUserId, name) > 0;
             session.commit();
             return contains;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return false;
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final SqlSessionFactory factory =
                serviceLocator.getSqlSessionFactoryService().getSqlSessionFactory();
        if (factory == null) throw new Exception();
        @Nullable final SqlSession session = factory.openSession();
        if (session == null) throw new Exception();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
             @Nullable final boolean isEmpty =
                     repository.isEmptyTaskRepository(currentUserId) > 0;
             session.commit();
             return isEmpty;
            } catch (SQLException e) {session.rollback();}
              finally {session.close();}
        return true;
    }

}
