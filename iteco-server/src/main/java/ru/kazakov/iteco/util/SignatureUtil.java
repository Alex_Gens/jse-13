package ru.kazakov.iteco.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SignatureUtil {

    @Nullable
    public static String getSignature(@Nullable final Object value) throws Exception {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            @NotNull final String signature = Password.getHashedPassword(json);
            return signature;
        } catch (final JsonProcessingException e) {return null;}
    }

}
