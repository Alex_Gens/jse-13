package ru.kazakov.iteco.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.constant.Constant;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

public final class Password {

    @NotNull
    public static String getHashedPassword(@Nullable final String password) throws Exception {
        if (password == null) throw new Exception();
        @NotNull String hashed = password;
        for (int i = 0; i < Constant.CYCLE; i++) {
            hashed = Constant.SALT + hashed + Constant.SALT;
            hashed = md5Custom(hashed);
        }
        return hashed;
    }

    @NotNull
    private static String md5Custom(@Nullable final String password) throws Exception {
        if (password == null) throw new Exception();
        @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        final byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }

}
