package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    public void persistUser(@NotNull final User entity) throws Exception;

    public void mergeUser(@NotNull final User entity) throws Exception;

    public void removeUser(@NotNull final String id) throws Exception;

    public void removeAllUsers() throws Exception;

    @Nullable
    public User findOneUser(@NotNull final String id) throws Exception;

    @Nullable
    public User findUserByLogin(@NotNull final String login) throws Exception;

    @NotNull
    public List<User> findAllUsers() throws Exception;

    @NotNull
    public List<String> findAllUsersLogins() throws SQLException;

    public int containsUserByLogin(@NotNull final String login) throws Exception;

}
