package ru.kazakov.iteco.api.context;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.api.service.*;
import ru.kazakov.iteco.service.SqlSessionFactoryService;

public interface ServiceLocator {

    @NotNull
    public IProjectService getProjectService();

    @NotNull
    public ITaskService getTaskService();

    @NotNull
    public IUserService getUserService();

    @NotNull
    public IDomainService getDomainService();

    @NotNull
    public ISessionService getSessionService();

    @NotNull
    public IPropertyService getPropertyService();

    @NotNull
    public SqlSessionFactoryService getSqlSessionFactoryService();

}
