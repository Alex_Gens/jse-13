package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    public String getHost();

    @NotNull
    public String getPort();

    @NotNull
    public String getSalt();

    @NotNull
    public String getCycle();

    @NotNull
    public String getSessionTime();

    @NotNull
    public String getSecret();

    @NotNull
    public String getDataBaseDriver();

    @NotNull
    public String getDataBaseUrl();

    @NotNull
    public String getDataBaseUser();

    @NotNull
    public String getDataBasePassword();

}
