package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;
import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {

    public void persistTask(@NotNull final Task entity) throws Exception;

    public void mergeTask(@NotNull final Task entity) throws Exception;

    public void removeTask(@NotNull final String id) throws Exception;

    public void removeTasksWithProject(
            @NotNull final String currentUserId,
            @NotNull final String projectId
    ) throws SQLException;

    public void removeAllTasks() throws Exception;

    public void removeAllTasksByCurrentId(@NotNull final String currentUserId) throws SQLException;

    public void removeAllTasksWithProjects(@NotNull final String currentUserId) throws SQLException;

    @Nullable
    public Task findOneTask(@NotNull final String id) throws Exception;

    @Nullable
    public Task findTaskByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name
    ) throws SQLException;

    @NotNull
    public List<Task> findAllTasks() throws Exception;

    @NotNull
    public List<Task> findAllTasksByCurrentId(@NotNull final String currentUserId) throws SQLException;

    @NotNull
    public List<String> findAllSortedTasksByCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String orderBy
    ) throws SQLException;

    @NotNull
    public List<String> findAllSortedTasksByCurrentIdProjectId(
            @NotNull final String currentUserId,
            @NotNull final String projectId,
            @NotNull final String orderBy
    ) throws SQLException;
    @NotNull
    public List<String> findAllTasksByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part
    ) throws SQLException;

    @NotNull
    public List<String> findAllTasksByInfoCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part
    ) throws SQLException;

    public int containsTaskByName(@NotNull final String name) throws SQLException;

    public int containsTaskByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name
    ) throws SQLException;

    public int isEmptyTaskRepository(@NotNull final String currentUserId) throws SQLException;

}
