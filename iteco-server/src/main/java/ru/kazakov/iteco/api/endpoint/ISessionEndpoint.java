package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.enumeration.RoleType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ISessionEndpoint {

    @NotNull
    @WebMethod
    public RoleType getSessionRoleType(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public String getInstanceToken(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception;

    @WebMethod
    public void persistSession(@Nullable final Session entity) throws Exception;

    @WebMethod
    public void removeSession(
            @Nullable final String id
    ) throws Exception;

    @WebMethod
    public void removeSessionByUserId(@Nullable final String userId) throws Exception;

    @WebMethod
    public void removeAllSessions() throws Exception;

    @Nullable
    @WebMethod
    public Session findOneSession(
            @Nullable final String id
    ) throws Exception;

    @Nullable
    @WebMethod
    public List<Session> findAllSessions() throws Exception;

    @WebMethod
    public boolean containsSession(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

}
