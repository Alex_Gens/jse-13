package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public interface ITaskService extends IService<Task> {

    public void merge(@Nullable final Task entity) throws Exception;

    public void create(
            @Nullable final String currentId,
            @Nullable final String name
    ) throws Exception;

    public void removeWithProject(
            @Nullable final String currentUserId,
            @Nullable final String projectId
    ) throws Exception;

    public void removeAll(@Nullable final String currentUserId) throws Exception;

    public void removeAllWithProjects(@Nullable final String currentUserId) throws Exception;

    @Nullable
    public Task findByName(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception;

    @Nullable
    public List<Task> findAll(@Nullable final String currentUserId) throws Exception;

    @Nullable
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final SortType sortType
    ) throws Exception;

    @Nullable
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final String projectId,
            @Nullable final SortType sortType
    ) throws Exception;

    @Nullable
    public List<String> findAllByName(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception;

    @Nullable
    public List<String> findAllByInfo(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception;

    public boolean contains(@Nullable final String name) throws Exception;

    public boolean contains(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception;

    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception;

}
