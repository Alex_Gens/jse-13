package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    public void mergeTask(
            @Nullable final String token,
            @Nullable final Task entity
    ) throws Exception;

    @WebMethod
    public void persistTask(
            @Nullable final String token,
            @Nullable final Task entity
    ) throws Exception;

    @WebMethod
    public void createTask(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception;

    @WebMethod
    public void removeTaskById(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception;

    @WebMethod
    public void removeTasksWithProject(
            @Nullable final String token,
            @Nullable final String projectId
    ) throws Exception;

    @WebMethod
    public void removeAllTasks(@Nullable final String token) throws Exception;

    @WebMethod
    public void removeAllTasksByCurrentId(@Nullable final String token) throws Exception;

    @WebMethod
    public void removeAllTasksWithProjects(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public Task findByTaskNameCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception;

    @Nullable
    @WebMethod
    public Task findOneTask(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception;

    @Nullable
    @WebMethod
    public List<Task> findAllTasks(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllSortedTasksByCurrentIdProjectId(
            @Nullable final String token,
            @Nullable final String projectId,
            @Nullable final SortType sortType
    ) throws Exception;

    @Nullable
    @WebMethod
    public List<Task> findAllTasksByCurrentId(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllSortedTasksByCurrentId(
            @Nullable final String token,
            @Nullable final SortType sortType
    ) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllTasksByNameCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllTasksByInfoCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception;

    @WebMethod
    public boolean containsTask(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception;

    @WebMethod
    public boolean containsTaskByCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception;

    @WebMethod
    public boolean isEmptyTaskRepositoryByCurrentId(@Nullable final String token) throws Exception;

}
