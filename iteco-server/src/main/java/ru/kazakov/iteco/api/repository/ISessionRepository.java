package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Session;
import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository {

    public void persistSession(@NotNull final Session entity) throws Exception;

    public void removeSession(@NotNull final String id) throws Exception;

    public void removeSessionByUserId(@NotNull final String userId);

    public void removeAllSessions() throws Exception;

    @Nullable
    public Session findOneSession(@NotNull final String id) throws Exception;

    @NotNull
    public List<Session> findAllSessions() throws Exception;

    public int containsSession(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException;

}
