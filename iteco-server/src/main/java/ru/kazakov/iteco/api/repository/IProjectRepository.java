package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;
import java.util.List;

public interface IProjectRepository {

    public void persistProject(@NotNull final Project entity) throws Exception;

    public void mergeProject(@NotNull final Project entity) throws Exception;

    public void removeProject(@NotNull final String id) throws Exception;

    public void removeAllProjects() throws Exception;

    public void removeAllProjectsByCurrentId(@NotNull final String currentUserId) throws Exception;

    @Nullable
    public Project findOneProject(@NotNull final String id) throws Exception;

    @Nullable
    public Project findByProjectNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name
    ) throws Exception;

    @NotNull
    public List<Project> findAllProjects() throws Exception;

    @NotNull
    public List<Project> findAllProjectsByCurrentId(@NotNull final String currentUserId) throws Exception;

    @NotNull
    public List<String> findAllSortedProjectsByCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String orderBy
    ) throws Exception;

    @NotNull
    public List<String> findAllProjectsByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part
    ) throws Exception;

    @NotNull
    public List<String> findAllProjectsByInfoCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part
    ) throws Exception;

    public int containsProjectByName(@NotNull final String name) throws Exception;

    public int containsProjectByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name
    ) throws Exception;

    public int isEmptyProjectRepository(@NotNull final String currentUserId) throws Exception;

}
