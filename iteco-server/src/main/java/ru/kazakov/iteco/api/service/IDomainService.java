package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;

public interface IDomainService {

    public void saveBin(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void saveJaxbXml(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void saveJaxbJson(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void saveFasterXml(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void saveFasterJson(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadBin(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadJaxbXml(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadJaxbJson(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadFasterXml(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadFasterJson(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public boolean isDirectory(@Nullable final String directory) throws Exception;

    public boolean exist(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

}
