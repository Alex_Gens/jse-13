package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.Nullable;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    public void saveDomainBin(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public void saveDomainJaxbXml(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public void saveDomainJaxbJson(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public void saveDomainFasterXml(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public void saveDomainFasterJson(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public void loadDomainBin(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public void loadDomainJaxbXml(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public void loadDomainJaxbJson(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public void loadDomainFasterXml(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public void loadDomainFasterJson(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @WebMethod
    public boolean isDomainDirectory(
            @Nullable final String token,
            @Nullable final String directory
    ) throws Exception;

    @WebMethod
    public boolean existDomain(
            @Nullable final String token,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

}
