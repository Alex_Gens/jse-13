package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    public void mergeUser(
            @Nullable final String token,
            @Nullable final User entity
    ) throws Exception;

    @WebMethod
    public void persistUser(@Nullable final User entity) throws Exception;

    @WebMethod
    public void removeUser(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception;

    @WebMethod
    public void removeAllUsers(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public User findOneUser(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception;

    @Nullable
    @WebMethod
    public User findUserByLogin(
            @Nullable final String token,
            @Nullable final String login
    ) throws Exception;

    @Nullable
    @WebMethod
    public User findCurrentUser(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public List<User> findAllUsers(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllUsersLogins(@Nullable final String token) throws Exception;

    @WebMethod
    public boolean containsUser(@Nullable final String login) throws Exception;

}
