package ru.kazakov.iteco;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.kazakov.iteco.api.endpoint.*;
import ru.kazakov.iteco.util.Password;
import java.lang.Exception;
import java.util.GregorianCalendar;
import java.util.UUID;

public class UserTest extends AbstractTest {

    @Test
    public void persistUser() throws Exception {
        @NotNull final String login = "test";
        @NotNull final String password = Password.getHashedPassword("test");
        @NotNull final User entity = new User();
        entity.setId(UUID.randomUUID().toString());
        entity.setLogin(login);
        entity.setPassword(password);
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setRoleType(RoleType.DEFAULT);
        userEndpoint.persistUser(entity);
        @Nullable final User entityFromDB = userEndpoint.findOneUser(defaultToken, entity.getId());
        Assert.assertNotNull(entityFromDB);
        userEndpoint.removeUser(defaultToken, entity.getId());
    }

    @Test
    public void mergeUser() throws Exception {
        @NotNull final String login = "test";
        @NotNull final String password = Password.getHashedPassword("test");
        @NotNull final User entity = new User();
        @NotNull final String name = "name";
        entity.setId(UUID.randomUUID().toString());
        entity.setLogin(login);
        entity.setPassword(password);
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setRoleType(RoleType.DEFAULT);
        userEndpoint.persistUser(entity);
        @Nullable final User entityFromDB = userEndpoint.findOneUser(defaultToken, entity.getId());
        Assert.assertNotNull(entityFromDB);
        entityFromDB.setName(name);
        userEndpoint.mergeUser(defaultToken, entityFromDB);
        @Nullable final User updatedEntity = userEndpoint.findOneUser(defaultToken, entity.getId());
        Assert.assertNotNull(updatedEntity);
        Assert.assertEquals(entityFromDB.getName(), updatedEntity.getName());
        userEndpoint.removeUser(defaultToken, entity.getId());
    }

    @Test
    public void removeUser() throws Exception {
        @NotNull final String login = "test";
        @NotNull final String password = Password.getHashedPassword("test");
        @NotNull final User entity = new User();
        @NotNull final String name = "name";
        entity.setId(UUID.randomUUID().toString());
        entity.setLogin(login);
        entity.setPassword(password);
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setRoleType(RoleType.DEFAULT);
        userEndpoint.persistUser(entity);
        @Nullable final User entityFromDB = userEndpoint.findOneUser(defaultToken, entity.getId());
        Assert.assertNotNull(entityFromDB);
        userEndpoint.removeUser(defaultToken, entityFromDB.getId());
        @Nullable final User removedEntity = userEndpoint.findOneUser(defaultToken, entity.getId());
        Assert.assertNull(removedEntity);
    }

    @Test
    public void findOneUser() throws Exception {
        @NotNull final String login = "test";
        @NotNull final String password = Password.getHashedPassword("test");
        @NotNull final User entity = new User();
        @NotNull final String name = "name";
        entity.setId(UUID.randomUUID().toString());
        entity.setLogin(login);
        entity.setPassword(password);
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setRoleType(RoleType.DEFAULT);
        userEndpoint.persistUser(entity);
        @Nullable final User entityFromDB = userEndpoint.findOneUser(defaultToken, entity.getId());
        Assert.assertNotNull(entityFromDB);
        Assert.assertEquals(entity.getId(), entityFromDB.getId());
        userEndpoint.removeUser(defaultToken, entity.getId());
    }

    @Test
    public void findUserByLogin() throws Exception {
        @NotNull final String login = "test";
        @NotNull final String password = Password.getHashedPassword("test");
        @NotNull final User entity = new User();
        @NotNull final String name = "name";
        entity.setId(UUID.randomUUID().toString());
        entity.setLogin(login);
        entity.setPassword(password);
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setRoleType(RoleType.DEFAULT);
        userEndpoint.persistUser(entity);
        @Nullable final User entityFromDB = userEndpoint.findUserByLogin(defaultToken, login);
        Assert.assertNotNull(entityFromDB);
        Assert.assertEquals(entity.getId(), entityFromDB.getId());
        userEndpoint.removeUser(defaultToken, entity.getId());
    }

    @Test
    public void containsUserByLogin() throws Exception {
        @NotNull final String login = "test";
        @NotNull final String password = Password.getHashedPassword("test");
        @NotNull final User entity = new User();
        @NotNull final String name = "name";
        entity.setId(UUID.randomUUID().toString());
        entity.setLogin(login);
        entity.setPassword(password);
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setRoleType(RoleType.DEFAULT);
        userEndpoint.persistUser(entity);
        final boolean contains = userEndpoint.containsUser(login);
        Assert.assertTrue(contains);
        userEndpoint.removeUser(defaultToken, entity.getId());
    }

}
