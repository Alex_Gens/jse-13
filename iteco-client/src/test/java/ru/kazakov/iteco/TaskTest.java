package ru.kazakov.iteco;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.kazakov.iteco.api.endpoint.Exception_Exception;
import ru.kazakov.iteco.api.endpoint.Status;
import ru.kazakov.iteco.api.endpoint.Task;
import ru.kazakov.iteco.api.endpoint.SortType;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

public class TaskTest extends AbstractTest {

    @After
    public void removeAfter() throws Exception {
        taskEndpoint.removeAllTasksByCurrentId(defaultToken);
        taskEndpoint.removeAllTasksByCurrentId(adminToken);
    }

    @Test
    public void persistTask() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final Task entity = new Task();
        entity.setId(UUID.randomUUID().toString());
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setUserId(defaultId);
        entity.setName(name);
        entity.setStatus(Status.PLANNED);
        taskEndpoint.persistTask(defaultToken, entity);
        @NotNull final Task entityFromDB = taskEndpoint.findOneTask(defaultToken, entity.getId());
        Assert.assertNotNull(entityFromDB);
    }

    @Test
    public void mergeTask() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final String information = "Some information";
        taskEndpoint.createTask(defaultToken, name);
        @Nullable final Task entity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name);
        Assert.assertNotNull(entity);
        entity.setInfo(information);
        taskEndpoint.mergeTask(defaultToken, entity);
        @Nullable final Task updatedEntity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name);
        Assert.assertNotNull(updatedEntity);
        Assert.assertEquals(entity.getInfo(), updatedEntity.getInfo());
    }

    @Test
    public void removeTask() throws Exception_Exception {
        @NotNull final String name = "test";
        taskEndpoint.createTask(defaultToken, name);
        @Nullable final Task entity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name);
        Assert.assertNotNull(entity);
        taskEndpoint.removeTaskById(defaultToken, entity.getId());
        @Nullable final Task removedEntity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name);
        Assert.assertNull(removedEntity);
    }

    @Test
    public void removeTasksWithProject() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final String projectId = UUID.randomUUID().toString();
        @NotNull final String secondProjectId = UUID.randomUUID().toString();
        taskEndpoint.createTask(defaultToken, name);
        taskEndpoint.createTask(defaultToken, name + name);
        taskEndpoint.createTask(defaultToken, name + name + name);
        taskEndpoint.createTask(defaultToken, name + name + name + name);
        @NotNull final Task entity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name);
        entity.setProjectId(projectId);
        taskEndpoint.mergeTask(defaultToken, entity);
        @NotNull final Task secondEntity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name + name);
        secondEntity.setProjectId(projectId);
        taskEndpoint.mergeTask(defaultToken, secondEntity);
        @NotNull final Task thirdEntity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name + name + name);
        thirdEntity.setProjectId(secondProjectId);
        taskEndpoint.mergeTask(defaultToken, thirdEntity);
        taskEndpoint.removeTasksWithProject(defaultToken, projectId);
        @NotNull final List<Task> list = taskEndpoint.findAllTasksByCurrentId(defaultToken);
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void removeAllTasksByCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        final int defaultTasksCount = 7;
        final int adminTasksCount = 5;
        for (int i = 0; i < defaultTasksCount ; i++) {
            taskEndpoint.createTask(defaultToken, name + i);
        }
        for (int i = 0; i < adminTasksCount ; i++) {
            taskEndpoint.createTask(adminToken, name + i);
        }
        List<Task> list = taskEndpoint.findAllTasks(defaultToken);
        Assert.assertEquals(defaultTasksCount + adminTasksCount, list.size());
        taskEndpoint.removeAllTasksByCurrentId(defaultToken);
        List<Task> listMustBeOnlyAdmin = taskEndpoint.findAllTasks(defaultToken);
        Assert.assertEquals(adminTasksCount, listMustBeOnlyAdmin.size());
    }

    @Test
    public void removeAllTasksWithProjects() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final String projectId = UUID.randomUUID().toString();
        @NotNull final String secondProjectId = UUID.randomUUID().toString();
        taskEndpoint.createTask(defaultToken, name);
        taskEndpoint.createTask(defaultToken, name + name);
        taskEndpoint.createTask(defaultToken, name + name + name);
        taskEndpoint.createTask(defaultToken, name + name + name + name);
        @NotNull final Task entity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name);
        entity.setProjectId(projectId);
        taskEndpoint.mergeTask(defaultToken, entity);
        @NotNull final Task secondEntity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name + name);
        secondEntity.setProjectId(projectId);
        taskEndpoint.mergeTask(defaultToken, secondEntity);
        @NotNull final Task thirdEntity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name + name + name);
        thirdEntity.setProjectId(secondProjectId);
        taskEndpoint.mergeTask(defaultToken, thirdEntity);
        taskEndpoint.removeAllTasksWithProjects(defaultToken);
        @NotNull final List<Task> list = taskEndpoint.findAllTasksByCurrentId(defaultToken);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void findOneTask() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final Task entity = new Task();
        entity.setId(UUID.randomUUID().toString());
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setName(name);
        entity.setUserId(defaultId);
        entity.setStatus(Status.PLANNED);
        taskEndpoint.persistTask(defaultToken, entity);
        @NotNull final Task entityFromDB = taskEndpoint.findOneTask(defaultToken, entity.getId());
        Assert.assertEquals(entity.getId(), entityFromDB.getId());
    }

    @Test
    public void findByTaskNameCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final Task defaultEntity = new Task();
        @NotNull final Task adminEntity = new Task();
        defaultEntity.setId(UUID.randomUUID().toString());
        defaultEntity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        defaultEntity.setName(name);
        defaultEntity.setUserId(defaultId);
        defaultEntity.setStatus(Status.PLANNED);
        adminEntity.setId(UUID.randomUUID().toString());
        adminEntity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        adminEntity.setName(name);
        adminEntity.setUserId(adminId);
        defaultEntity.setStatus(Status.PLANNED);
        taskEndpoint.persistTask(defaultToken, defaultEntity);
        taskEndpoint.persistTask(adminToken, adminEntity);
        @NotNull final Task defaultEntityFromDB = taskEndpoint.findByTaskNameCurrentId(defaultToken, name);
        @NotNull final Task adminEntityFromDB = taskEndpoint.findByTaskNameCurrentId(adminToken, name);
        Assert.assertEquals(defaultEntity.getId(), defaultEntityFromDB.getId());
        Assert.assertEquals(adminEntity.getId(), adminEntityFromDB.getId());
    }


    @Test
    public void findAllTasksByCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        final int defaultTasksCount = 7;
        final int adminTasksCount = 5;
        for (int i = 0; i < defaultTasksCount ; i++) {
            taskEndpoint.createTask(defaultToken, name + i);
        }
        for (int i = 0; i < adminTasksCount ; i++) {
            taskEndpoint.createTask(adminToken, name + i);
        }
        List<Task> list = taskEndpoint.findAllTasksByCurrentId(defaultToken);
        Assert.assertEquals(list.size(), defaultTasksCount);
    }

    @Test
    public void findAllSortedTasksByCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final SortType sortType = SortType.CREATED;
        final int defaultTasksCount = 7;
        final int adminTasksCount = 5;
        for (int i = 0; i < defaultTasksCount ; i++) {
            taskEndpoint.createTask(defaultToken, name + i);
        }
        for (int i = 0; i < adminTasksCount ; i++) {
            taskEndpoint.createTask(adminToken, name + i);
        }
        List<String> list = taskEndpoint.findAllSortedTasksByCurrentId(defaultToken, sortType);
        Assert.assertEquals(list.size(), defaultTasksCount);
    }

    @Test
    public void findAllSortedTasksByCurrentIdProjectId() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final String secondName = "name";
        @NotNull final SortType sortType = SortType.CREATED;
        @NotNull final String projectId = UUID.randomUUID().toString();
        @NotNull final String secondProjectId = UUID.randomUUID().toString();
        final int defaultTasksCount = 7;
        final int adminTasksCount = 5;
        for (int i = 0; i < defaultTasksCount ; i++) {
            taskEndpoint.createTask(defaultToken, name + i);
            @NotNull final Task entity = taskEndpoint.findByTaskNameCurrentId(defaultToken, name + i);
            entity.setProjectId(projectId);
            taskEndpoint.mergeTask(defaultToken, entity);
        }
        for (int i = 0; i < defaultTasksCount ; i++) {
            taskEndpoint.createTask(defaultToken, secondName + i);
            @NotNull final Task entity = taskEndpoint.findByTaskNameCurrentId(defaultToken, secondName + i);
            entity.setProjectId(secondProjectId);
            taskEndpoint.mergeTask(defaultToken, entity);
        }
        for (int i = 0; i < adminTasksCount ; i++) {
            taskEndpoint.createTask(adminToken, name + i);
            @NotNull final Task entity = taskEndpoint.findByTaskNameCurrentId(adminToken, name + i);
            entity.setProjectId(secondProjectId);
            taskEndpoint.mergeTask(adminToken, entity);
        }
        for (int i = 0; i < adminTasksCount ; i++) {
            taskEndpoint.createTask(adminToken, secondName + i);
            @NotNull final Task entity = taskEndpoint.findByTaskNameCurrentId(adminToken, secondName + i);
            entity.setProjectId(secondProjectId);
            taskEndpoint.mergeTask(adminToken, entity);
        }
        List<String> list = taskEndpoint.findAllSortedTasksByCurrentIdProjectId(defaultToken, projectId, sortType);
        Assert.assertEquals(list.size(), defaultTasksCount);
    }

    @Test
    public void findAllTasksByNameCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final String secondName = "name";
        @NotNull final String part = "es";
        final int defaultTasksCount = 7;
        final int adminTasksCount = 5;
        for (int i = 0; i < defaultTasksCount ; i++) {
            taskEndpoint.createTask(defaultToken, name + i);
        }
        for (int i = 0; i < defaultTasksCount ; i++) {
            taskEndpoint.createTask(defaultToken, secondName + i);
        }
        for (int i = 0; i < adminTasksCount ; i++) {
            taskEndpoint.createTask(adminToken, name + i);
        }
        for (int i = 0; i < adminTasksCount ; i++) {
            taskEndpoint.createTask(adminToken, secondName + i);
        }
        List<String> list = taskEndpoint.findAllTasksByNameCurrentId(defaultToken, part);
        Assert.assertEquals(list.size(), defaultTasksCount);
    }

    @Test
    public void findAllTasksByInfoCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final String information = "Some information";
        @NotNull final String secondInformation = "Other";
        @NotNull final String part = "info";
        final int defaultTasksCount = 7;
        final int adminTasksCount = 5;
        for (int i = 0; i < defaultTasksCount ; i++) {
            @NotNull final Task entity = new Task();
            entity.setId(UUID.randomUUID().toString());
            entity.setUserId(defaultId);
            entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
            entity.setName(name + i);
            entity.setInfo(information);
            entity.setStatus(Status.PLANNED);
            taskEndpoint.persistTask(defaultToken, entity);
        }
        for (int i = 0; i < adminTasksCount ; i++) {
            @NotNull final Task entity = new Task();
            entity.setId(UUID.randomUUID().toString());
            entity.setUserId(adminId);
            entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
            entity.setName(name + i);
            entity.setInfo(information);
            entity.setStatus(Status.PLANNED);
            taskEndpoint.persistTask(adminToken, entity);
        }
        for (int i = 0; i < defaultTasksCount ; i++) {
            @NotNull final Task entity = new Task();
            entity.setId(UUID.randomUUID().toString());
            entity.setUserId(defaultId);
            entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
            entity.setName(name + i);
            entity.setInfo(secondInformation);
            entity.setStatus(Status.PLANNED);
            taskEndpoint.persistTask(defaultToken, entity);
        }
        for (int i = 0; i < adminTasksCount ; i++) {
            @NotNull final Task entity = new Task();
            entity.setId(UUID.randomUUID().toString());
            entity.setUserId(adminId);
            entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
            entity.setName(name + i);
            entity.setInfo(secondInformation);
            entity.setStatus(Status.PLANNED);
            taskEndpoint.persistTask(adminToken, entity);
        }
        List<String> list = taskEndpoint.findAllTasksByInfoCurrentId(defaultToken, part);
        Assert.assertEquals(list.size(), defaultTasksCount);
    }

    @Test
    public void containsTaskByName() throws Exception_Exception {
        @NotNull final String name = "test";
        taskEndpoint.createTask(defaultToken, name);
        final boolean contains = taskEndpoint.containsTask(defaultToken, name);
        Assert.assertTrue(contains);
    }

    @Test
    public void containsTaskByNameCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        taskEndpoint.createTask(adminToken, name);
        final boolean containsFalse = taskEndpoint.containsTaskByCurrentId(defaultToken, name);
        Assert.assertFalse(containsFalse);
        final boolean containsTrue = taskEndpoint.containsTaskByCurrentId(adminToken, name);
        Assert.assertTrue(containsTrue);
    }

    @Test
    public void isEmptyTaskRepository() throws Exception_Exception {
        @NotNull final String name = "test";
        Assert.assertTrue(taskEndpoint.isEmptyTaskRepositoryByCurrentId(defaultToken));
        Assert.assertTrue(taskEndpoint.isEmptyTaskRepositoryByCurrentId(adminToken));
        taskEndpoint.createTask(defaultToken, name);
        Assert.assertFalse(taskEndpoint.isEmptyTaskRepositoryByCurrentId(defaultToken));
        Assert.assertTrue(taskEndpoint.isEmptyTaskRepositoryByCurrentId(adminToken));
    }

}
