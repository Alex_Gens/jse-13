package ru.kazakov.iteco;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.kazakov.iteco.api.endpoint.Exception_Exception;
import ru.kazakov.iteco.api.endpoint.Project;
import ru.kazakov.iteco.api.endpoint.SortType;
import ru.kazakov.iteco.api.endpoint.Status;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

public class ProjectTest extends AbstractTest {

    @After
    public void removeAfter() throws Exception {
        projectEndpoint.removeAllProjectsByCurrentId(defaultToken);
        projectEndpoint.removeAllProjectsByCurrentId(adminToken);
    }

    @Test
    public void persistProject() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final Project entity = new Project();
        entity.setId(UUID.randomUUID().toString());
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setUserId(defaultId);
        entity.setName(name);
        entity.setStatus(Status.PLANNED);
        projectEndpoint.persistProject(defaultToken, entity);
        @NotNull final Project entityFromDB = projectEndpoint.findOneProject(defaultToken, entity.getId());
        Assert.assertNotNull(entityFromDB);
    }

    @Test
    public void mergeProject() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final String information = "Some information";
        projectEndpoint.createProject(defaultToken, name);
        @Nullable final Project entity = projectEndpoint.findByProjectNameCurrentId(defaultToken, name);
        Assert.assertNotNull(entity);
        entity.setInfo(information);
        projectEndpoint.mergeProject(defaultToken, entity);
        @Nullable final Project updatedEntity = projectEndpoint.findByProjectNameCurrentId(defaultToken, name);
        Assert.assertNotNull(updatedEntity);
        Assert.assertEquals(entity.getInfo(), updatedEntity.getInfo());
    }

    @Test
    public void removeProject() throws Exception_Exception {
        @NotNull final String name = "test";
        projectEndpoint.createProject(defaultToken, name);
        @Nullable final Project entity = projectEndpoint.findByProjectNameCurrentId(defaultToken, name);
        Assert.assertNotNull(entity);
        projectEndpoint.removeProjectById(defaultToken, entity.getId());
        @Nullable final Project removedEntity = projectEndpoint.findByProjectNameCurrentId(defaultToken, name);
        Assert.assertNull(removedEntity);
    }

    @Test
    public void removeAllProjectsByCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        final int defaultProjectsCount = 7;
        final int adminProjectsCount = 5;
        for (int i = 0; i < defaultProjectsCount ; i++) {
            projectEndpoint.createProject(defaultToken, name + i);
        }
        for (int i = 0; i < adminProjectsCount ; i++) {
            projectEndpoint.createProject(adminToken, name + i);
        }
        List<Project> list = projectEndpoint.findAllProjects(defaultToken);
        Assert.assertEquals(defaultProjectsCount + adminProjectsCount, list.size());
        projectEndpoint.removeAllProjectsByCurrentId(defaultToken);
        List<Project> listMustBeOnlyAdmin = projectEndpoint.findAllProjects(defaultToken);
        Assert.assertEquals(adminProjectsCount, listMustBeOnlyAdmin.size());
    }

    @Test
    public void findOneProject() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final Project entity = new Project();
        entity.setId(UUID.randomUUID().toString());
        entity.setUserId(defaultId);
        entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        entity.setName(name);
        entity.setStatus(Status.PLANNED);
        projectEndpoint.persistProject(defaultToken, entity);
        @NotNull final Project entityFromDB = projectEndpoint.findOneProject(defaultToken, entity.getId());
        Assert.assertEquals(entity.getId(), entityFromDB.getId());
    }

    @Test
    public void findByProjectNameCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final Project defaultEntity = new Project();
        @NotNull final Project adminEntity = new Project();
        defaultEntity.setId(UUID.randomUUID().toString());
        defaultEntity.setUserId(defaultId);
        defaultEntity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        defaultEntity.setName(name);
        defaultEntity.setStatus(Status.PLANNED);
        adminEntity.setId(UUID.randomUUID().toString());
        adminEntity.setUserId(adminId);
        adminEntity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        adminEntity.setName(name);
        adminEntity.setStatus(Status.PLANNED);
        projectEndpoint.persistProject(defaultToken, defaultEntity);
        projectEndpoint.persistProject(adminToken, adminEntity);
        @NotNull final Project defaultEntityFromDB = projectEndpoint.findByProjectNameCurrentId(defaultToken, name);
        @NotNull final Project adminEntityFromDB = projectEndpoint.findByProjectNameCurrentId(adminToken, name);
        Assert.assertEquals(defaultEntity.getId(), defaultEntityFromDB.getId());
        Assert.assertEquals(adminEntity.getId(), adminEntityFromDB.getId());
    }

    @Test
    public void findAllProjectsByCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        final int defaultProjectsCount = 7;
        final int adminProjectsCount = 5;
        for (int i = 0; i < defaultProjectsCount ; i++) {
            projectEndpoint.createProject(defaultToken, name + i);
        }
        for (int i = 0; i < adminProjectsCount ; i++) {
            projectEndpoint.createProject(adminToken, name + i);
        }
        List<Project> list = projectEndpoint.findAllProjectsByCurrentId(defaultToken);
        Assert.assertEquals(list.size(), defaultProjectsCount);
    }

    @Test
    public void findAllSortedProjectsByCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final SortType sortType = SortType.CREATED;
        final int defaultProjectsCount = 7;
        final int adminProjectsCount = 5;
        for (int i = 0; i < defaultProjectsCount ; i++) {
            projectEndpoint.createProject(defaultToken, name + i);
        }
        for (int i = 0; i < adminProjectsCount ; i++) {
            projectEndpoint.createProject(adminToken, name + i);
        }
        List<String> list = projectEndpoint.findAllSortedProjectsByCurrentId(defaultToken, sortType);
        Assert.assertEquals(list.size(), defaultProjectsCount);
    }

    @Test
    public void findAllProjectsByNameCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final String secondName = "name";
        @NotNull final String part = "es";
        final int defaultProjectsCount = 7;
        final int adminProjectsCount = 5;
        for (int i = 0; i < defaultProjectsCount ; i++) {
            projectEndpoint.createProject(defaultToken, name + i);
        }
        for (int i = 0; i < defaultProjectsCount ; i++) {
            projectEndpoint.createProject(defaultToken, secondName + i);
        }
        for (int i = 0; i < adminProjectsCount ; i++) {
            projectEndpoint.createProject(adminToken, name + i);
        }
        for (int i = 0; i < adminProjectsCount ; i++) {
            projectEndpoint.createProject(adminToken, secondName + i);
        }
        List<String> list = projectEndpoint.findAllProjectsByNameCurrentId(defaultToken, part);
        Assert.assertEquals(list.size(), defaultProjectsCount);
    }

    @Test
    public void findAllProjectsByInfoCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        @NotNull final String information = "Some information";
        @NotNull final String secondInformation = "Other";
        @NotNull final String part = "info";
        final int defaultProjectsCount = 7;
        final int adminProjectsCount = 5;
        for (int i = 0; i < defaultProjectsCount ; i++) {
            @NotNull final Project entity = new Project();
            entity.setId(UUID.randomUUID().toString());
            entity.setUserId(defaultId);
            entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
            entity.setName(name + i);
            entity.setStatus(Status.PLANNED);
            entity.setInfo(information);
            projectEndpoint.persistProject(defaultToken, entity);
        }
        for (int i = 0; i < adminProjectsCount ; i++) {
            @NotNull final Project entity = new Project();
            entity.setId(UUID.randomUUID().toString());
            entity.setUserId(adminId);
            entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
            entity.setName(name + i);
            entity.setStatus(Status.PLANNED);
            entity.setInfo(information);
            projectEndpoint.persistProject(adminToken, entity);
        }
        for (int i = 0; i < defaultProjectsCount ; i++) {
            @NotNull final Project entity = new Project();
            entity.setId(UUID.randomUUID().toString());
            entity.setUserId(defaultId);
            entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
            entity.setName(name + i);
            entity.setStatus(Status.PLANNED);
            entity.setInfo(secondInformation);
            projectEndpoint.persistProject(defaultToken, entity);
        }
        for (int i = 0; i < adminProjectsCount ; i++) {
            @NotNull final Project entity = new Project();
            entity.setId(UUID.randomUUID().toString());
            entity.setUserId(adminId);
            entity.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
            entity.setName(name + i);
            entity.setStatus(Status.PLANNED);
            entity.setInfo(secondInformation);
            projectEndpoint.persistProject(adminToken, entity);
        }
        List<String> list = projectEndpoint.findAllProjectsByInfoCurrentId(defaultToken, part);
        Assert.assertEquals(list.size(), defaultProjectsCount);
    }

    @Test
    public void containsProjectByName() throws Exception_Exception {
        @NotNull final String name = "test";
        projectEndpoint.createProject(defaultToken, name);
        final boolean contains = projectEndpoint.containsProject(defaultToken, name);
        Assert.assertTrue(contains);
    }

    @Test
    public void containsProjectByNameCurrentId() throws Exception_Exception {
        @NotNull final String name = "test";
        projectEndpoint.createProject(adminToken, name);
        final boolean containsFalse = projectEndpoint.containsProjectByCurrentId(defaultToken, name);
        Assert.assertFalse(containsFalse);
        final boolean containsTrue = projectEndpoint.containsProjectByCurrentId(adminToken, name);
        Assert.assertTrue(containsTrue);
    }

    @Test
    public void isEmptyProjectRepository() throws Exception_Exception {
        @NotNull final String name = "test";
        Assert.assertTrue(projectEndpoint.isEmptyProjectRepositoryByCurrentId(defaultToken));
        Assert.assertTrue(projectEndpoint.isEmptyProjectRepositoryByCurrentId(adminToken));
        projectEndpoint.createProject(defaultToken, name);
        Assert.assertFalse(projectEndpoint.isEmptyProjectRepositoryByCurrentId(defaultToken));
        Assert.assertTrue(projectEndpoint.isEmptyProjectRepositoryByCurrentId(adminToken));
    }

}
