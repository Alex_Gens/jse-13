package ru.kazakov.iteco;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import ru.kazakov.iteco.api.endpoint.*;
import ru.kazakov.iteco.endpoint.ProjectEndpointService;
import ru.kazakov.iteco.endpoint.SessionEndpointService;
import ru.kazakov.iteco.endpoint.TaskEndpointService;
import ru.kazakov.iteco.endpoint.UserEndpointService;
import ru.kazakov.iteco.util.Password;
import java.lang.Exception;
import java.util.GregorianCalendar;
import java.util.UUID;

public abstract class AbstractTest {

    @NotNull
    protected static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    protected static final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    protected static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    protected static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    protected static String adminToken;

    @Nullable
    protected static String defaultToken;

    @Nullable
    protected static String adminId;

    @Nullable
    protected static String defaultId;

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        defaultId = user.getId();
        user.setLogin("defaultTest");
        @NotNull final String notHashedPassword = "test";
        @NotNull final String password = Password.getHashedPassword(notHashedPassword);
        user.setPassword(password);
        user.setRoleType(RoleType.DEFAULT);
        @NotNull final User admin = new User();
        admin.setId(UUID.randomUUID().toString());
        admin.setDateCreate(new XMLGregorianCalendarImpl(new GregorianCalendar()));
        adminId = admin.getId();
        admin.setLogin("adminTest");
        admin.setPassword(password);
        admin.setRoleType(RoleType.ADMINISTRATOR);
        userEndpoint.persistUser(user);
        userEndpoint.persistUser(admin);
        defaultToken = sessionEndpoint.getInstanceToken(user.getLogin(), notHashedPassword);
        adminToken = sessionEndpoint.getInstanceToken(admin.getLogin(), notHashedPassword);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        sessionEndpoint.removeSession(defaultId);
        sessionEndpoint.removeSession(adminId);
        userEndpoint.removeUser(defaultToken, defaultId);
        userEndpoint.removeUser(adminToken, adminId);
    }

}
