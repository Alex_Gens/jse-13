package ru.kazakov.iteco.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.IDomainEndpoint;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.command.AbstractCommand;

@NoArgsConstructor
public abstract class DataAbstractSaveCommand extends AbstractCommand {

    @Nullable
    protected IDomainEndpoint domainEndpoint;

    @NotNull
    protected final String directory = "data";

    @NotNull
    protected RoleType roleType = RoleType.ADMINISTRATOR;

    @Override
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
        this.domainEndpoint = serviceLocator.getDomainEndpoint();
    }

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

    protected boolean confirmed() throws Exception {
        if (terminalService == null) throw new Exception();
        @NotNull String answer;
        while (true) {
            terminalService.write("Last save-file will be overwritten. Continue?");
            terminalService.write("1. Yes");
            terminalService.write("2. No");
            terminalService.separateLines();
            terminalService.write("ENTER ANSWER NUMBER: ");
            answer = terminalService.enterIgnoreEmpty().trim();
            if (!answer.equals("1") && !answer.equals("2")) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("Enter correct number.");
                terminalService.separateLines();
                continue;
            }
            if (answer.equals("2")) {
                terminalService.write("[NOT SAVED]");
                terminalService.separateLines();
                return false;
            }
            break;
        }
        terminalService.separateLines();
        return true;
    }

}
