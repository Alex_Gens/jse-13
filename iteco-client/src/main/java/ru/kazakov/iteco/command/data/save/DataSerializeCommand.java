package ru.kazakov.iteco.command.data.save;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataSerializeCommand extends DataAbstractSaveCommand {

    @Getter
    @NotNull
    private final String name = "data-save-bin";

    @Getter
    @NotNull
    private final String description = "Save data in binary format.";

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (domainEndpoint == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        @NotNull final String fileName = "data.bin";
        if (token == null) throw new Exception();
        if (!confirmed()) return;
        domainEndpoint.saveDomainBin(token, directory, fileName);
        terminalService.write("[SAVED]");
        terminalService.write("Data successfully saved in " + fileName + "!");
        terminalService.separateLines();
    }

}
