package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Status;
import ru.kazakov.iteco.api.endpoint.Task;
import ru.kazakov.iteco.util.DateUtil;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;

@NoArgsConstructor
public final class TaskUpdateCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-update";

    @Getter
    @NotNull
    private final String description = "Update task information.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable Task task = getTaskByPart();
        if (task == null) return;
        Map<String, String> items = new HashMap<>();
        items.put(String.valueOf(1), "Task start date");
        items.put(String.valueOf(2), "Task finish date");
        items.put(String.valueOf(3), "Task status");
        items.put(String.valueOf(4), "Task information");
        @NotNull String number;
        while (true) {
            terminalService.write("Select item to update: ");
            items.forEach((k, v) -> terminalService.write(k + ": " + v));
            terminalService.separateLines();
            terminalService.write("ENTER ITEM NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!items.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of item doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.separateLines();
            break;
        }
        switch (number) {
            case "1" : task = updateDateStart(task); break;
            case "2" : task = updateDateFinish(task); break;
            case "3" : task = updateStatus(task); break;
            case "4" : task = updateInformation(task); break;
        }
        taskEndpoint.mergeTask(token, task);
        terminalService.write("[UPDATED]");
        terminalService.write("Task successfully updated!");
        terminalService.separateLines();
    }

    private Task updateDateStart(@NotNull final Task task) throws Exception {
        if (terminalService == null) throw new Exception();
        while (true) {
            terminalService.write("Enter new task's start date in dd.mm.yyyy format.");
            terminalService.write("ENTER START DATE:");
            @NotNull final String enteredDate = terminalService.enterIgnoreEmpty();
            @Nullable final Date date = DateUtil.parseDate(enteredDate);
            if (date == null) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("Incorrect date or format.");
                terminalService.separateLines();
                continue;
            }
            @NotNull final GregorianCalendar temp = new GregorianCalendar();
            temp.setTime(date);
            @NotNull final XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(temp);
            task.setDateStart(calendar);
            return task;
        }
    }

     private Task updateDateFinish(@NotNull final Task task) throws Exception {
        if (terminalService == null) throw new Exception();
        while (true) {
            terminalService.write("Enter new task's start date in dd.mm.yyyy format.");
            terminalService.write("ENTER FINISH DATE:");
            @NotNull final String enteredDate = terminalService.enterIgnoreEmpty();
            @Nullable final Date date = DateUtil.parseDate(enteredDate);
            if (date == null) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("Incorrect date or format.");
                terminalService.separateLines();
                continue;
            }
            @NotNull final GregorianCalendar temp = new GregorianCalendar();
            temp.setTime(date);
            @NotNull final XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(temp);
            task.setDateFinish(calendar);
            return task;
        }
    }

    private Task updateStatus(@NotNull final Task task) throws Exception {
        if (terminalService == null) throw new Exception();
        @NotNull final Map<String, Status> statusMap = new TreeMap<>();
        @NotNull final Status[] statuses = Status.values();
        for (int i = 0; i < statuses.length; i++) {
            statusMap.put(String.valueOf(i + 1), statuses[i]);
        }
        terminalService.write("Select status:");
        @NotNull String number;
        while (true) {
            statusMap.forEach((k, v) -> terminalService.write(k + ". " + v.value()));
            terminalService.separateLines();
            terminalService.write("ENTER STATUS NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!statusMap.containsKey(number)) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("This number of status doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            task.setStatus(statusMap.get(number));
            return task;
        }
    }

    private Task updateInformation(@NotNull final Task task) throws Exception {
        if (terminalService == null) throw new Exception();
        terminalService.write("Use \"-save\" to finish entering, and save information.");
        terminalService.write("ENTER task INFORMATION: ");
        @NotNull final String newInfo = terminalService.read();
        task.setInfo(newInfo);
        return task;
    }

}
