package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.RoleType;
import java.util.List;

@NoArgsConstructor
public final class UserListCommand extends  UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-list";

    @Getter
    @NotNull
    private final String description = "Show all users.";

    @NotNull
    private RoleType roleType = RoleType.ADMINISTRATOR;

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (userEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null) throw new Exception();
        @Nullable final List<String> users = userEndpoint.findAllUsersLogins(token);
        if (users == null || users.isEmpty()) {
            terminalService.write("User list is empty.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[USERS LIST]");
        int counter = 1;
        for (String userLogin : users) {
            System.out.println(counter + ". " + userLogin);
            counter++;
        }
        terminalService.separateLines();
    }

}
