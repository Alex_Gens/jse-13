package ru.kazakov.iteco.command;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.api.service.ITerminalService;

@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    @Setter
    @Nullable
    protected CurrentState currentState;

    @Nullable
    protected ITerminalService terminalService;

    @NotNull
    protected RoleType roleType = RoleType.DEFAULT;

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.terminalService = serviceLocator.getTerminalService();
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    @NotNull
    public RoleType getRoleType() {return this.roleType;}

}
