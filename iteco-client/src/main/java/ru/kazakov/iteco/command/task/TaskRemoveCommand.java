package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Task;

@NoArgsConstructor
public final class TaskRemoveCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-remove";

    @Getter
    @NotNull
    private final String description = "Remove task.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable final Task task = getTaskByPart();
        if (task == null) return;
        taskEndpoint.removeTaskById(token, task.getId());
        terminalService.write("[REMOVED]");
        terminalService.write("Task successfully removed!");
        terminalService.separateLines();
    }

}
