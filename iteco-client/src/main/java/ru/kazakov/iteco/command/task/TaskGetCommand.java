package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Task;

@NoArgsConstructor
public final class TaskGetCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-get";

    @Getter
    @NotNull
    private final String description = "Show all task information.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable final Task task = getTaskByPart();
        if (task == null) return;
        if (task.getInfo() == null || task.getInfo().isEmpty()) {
            terminalService.write("Task is empty. Use \"task-update\" to update this task.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[Task: " + task.getName() + "]");
        if (task.getInfo() == null) throw new Exception();
        terminalService.write(task.getInfo());
        terminalService.separateLines();
    }

}
