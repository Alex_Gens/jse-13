package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.ITaskEndpoint;
import ru.kazakov.iteco.api.endpoint.Project;
import ru.kazakov.iteco.api.endpoint.SortType;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@NoArgsConstructor
public final class ProjectListTasksCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-list-tasks";

    @Getter
    @NotNull
    private final String description = "Show all tasks in project.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable final Project project = getProjectByPart();
        if (project == null) return;
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final SortType[] sortTypes = SortType.values();
        Map<String, SortType> sortTypesMap = new TreeMap<>();
        for (int i = 0; i < sortTypes.length; i++) {
            sortTypesMap.put(String.valueOf(i + 1), sortTypes[i]);
        }
        terminalService.write("Select list sorting type");
        @NotNull String number;
        while (true) {
            sortTypesMap.forEach((k, v) -> terminalService.write(k + ". " + v));
            terminalService.separateLines();
            terminalService.write("ENTER SORTING TYPE NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!sortTypesMap.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of sorting type doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.separateLines();
            break;
        }
        @Nullable final List<String> tasksNames =
                taskEndpoint.findAllSortedTasksByCurrentIdProjectId(token, project.getId(), sortTypesMap.get(number));
        if (tasksNames == null) throw new Exception();
        if (tasksNames.isEmpty()) {
            terminalService.write("The project has no tasks. Use project-add-task to add task to project.");
            terminalService.separateLines();
            return;
        }
        int counter = 1;
        terminalService.write("[TASKS LIST]");
        for (String taskName : tasksNames) {
            terminalService.write(counter + ". " + taskName);
            counter++;
        }
        terminalService.separateLines();
    }

}
