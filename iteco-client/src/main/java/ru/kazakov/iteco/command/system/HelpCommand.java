package ru.kazakov.iteco.command.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.command.AbstractCommand;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    private final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "help";

    @Getter
    @NotNull
    private final String description = "Show all commands.";

    @NotNull
    private RoleType roleType = RoleType.NONE;

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        @NotNull final List<AbstractCommand> commands = currentState.getCommands();
        commands.sort(Comparator.comparing(AbstractCommand::getName));
        if (token != null) {
            @NotNull final RoleType currentUserRoleType = serviceLocator.getSessionEndpoint().getSessionRoleType(token);
            if (currentUserRoleType == RoleType.ADMINISTRATOR) {
                terminalService.write("[ADMINISTRATOR COMMANDS]");
                commands.forEach(v -> {if (v.getRoleType() == RoleType.ADMINISTRATOR)
                        terminalService.write(v.getName() + ": " + v.getDescription());});
                terminalService.separateLines();
            }
        }
        terminalService.write("[DEFAULT COMMANDS]");
        commands.forEach(v -> {if (v.getRoleType() != RoleType.ADMINISTRATOR)
            terminalService.write(v.getName() + ": " + v.getDescription());});
        terminalService.separateLines();
    }

}
