package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.ITaskEndpoint;
import ru.kazakov.iteco.api.endpoint.Project;
import ru.kazakov.iteco.api.endpoint.Task;

@NoArgsConstructor
public final class ProjectAddTaskCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-add-task";

    @Getter
    @NotNull
    private final String description = "Add task to project.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable final Project project = getProjectByPart();
        if (project == null) return;
        @Nullable final Task task = getTaskByPart();
        if (task == null) return;
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        if (task.getProjectId() == null || !task.getProjectId().equals(project.getId())) {
            task.setProjectId(project.getId());
            taskEndpoint.mergeTask(token, task);
            terminalService.write("Task successfully added!");
            terminalService.separateLines();
            return;
        }
        terminalService.write("Task is already added. Use project-list-tasks to see tasks in project.");
        terminalService.separateLines();
    }

}
