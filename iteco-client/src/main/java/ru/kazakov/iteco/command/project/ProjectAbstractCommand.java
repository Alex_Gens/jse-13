package ru.kazakov.iteco.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.*;
import ru.kazakov.iteco.command.AbstractCommand;
import java.lang.Exception;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@NoArgsConstructor
public abstract class ProjectAbstractCommand extends AbstractCommand {

    @Nullable
    protected IProjectEndpoint projectEndpoint;

    @Override
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
        this.projectEndpoint = serviceLocator.getProjectEndpoint();
    }

    @Nullable
    protected Project getProjectByPart() throws Exception {
        if (terminalService == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (currentState == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        terminalService.write("ENTER PART OF PROJECT NAME OR DESCRIPTION: ");
        @NotNull final String part = terminalService.enterIgnoreEmpty().trim();
        terminalService.separateLines();
        @Nullable final List<String> projectsByName = projectEndpoint.findAllProjectsByNameCurrentId(token, part);
        @Nullable final List<String> projectsByInfo = projectEndpoint.findAllProjectsByInfoCurrentId(token, part);
        if (projectsByName == null) throw new Exception();
        if (projectsByInfo == null) throw new Exception();
        if (projectsByName.isEmpty() && projectsByInfo.isEmpty()) {
            terminalService.write("Project doesn't exist. Use \"project-list\" to show all projects.");
            terminalService.separateLines();
            return null;
        }
        int counter = 1;
        @NotNull final Map<String, String> projectsByNameMap = new TreeMap<>();
        @NotNull final Map<String, String> projectsByInfoMap = new TreeMap<>();
        for (String projectName : projectsByName) {
            projectsByNameMap.put(String.valueOf(counter), projectName);
            counter++;
        }
        for (String projectName : projectsByInfo) {
            projectsByInfoMap.put(String.valueOf(counter), projectName);
            counter++;
        }
        @NotNull final Project project;
        while (true) {
            if (!projectsByNameMap.isEmpty()) terminalService.write("Found by name: ");
            projectsByNameMap.forEach((k, v) -> terminalService.write(k + ". " + v));
            if (!projectsByInfoMap.isEmpty()) terminalService.write("Found by description:");
            projectsByInfoMap.forEach((k, v) -> terminalService.write(k + ". " + v));
            terminalService.separateLines();
            terminalService.write("Select project.");
            terminalService.write("ENTER PROJECT NUMBER: ");
            @NotNull final String number = terminalService.enterIgnoreEmpty();
            if (!projectsByNameMap.containsKey(number) && !projectsByInfoMap.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of item doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            if (projectsByNameMap.containsKey(number)) {
                @NotNull final String projectName = projectsByNameMap.get(number);
                project = projectEndpoint.findByProjectNameCurrentId(token, projectName);
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                break;
            }
            if (projectsByInfoMap.containsKey(number)) {
                @NotNull final String projectName = projectsByNameMap.get(number);
                project = projectEndpoint.findByProjectNameCurrentId(token, projectName);
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                break;
            }
        }
        return project;
    }

    @Nullable
    protected Task getTaskByPart() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (currentState == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        terminalService.write("ENTER PART OF TASK NAME OR DESCRIPTION: ");
        @NotNull final String part = terminalService.enterIgnoreEmpty();
        terminalService.separateLines();
        @Nullable final List<String> tasksByName = taskEndpoint.findAllTasksByNameCurrentId(token, part);
        @Nullable final List<String> tasksByInfo = taskEndpoint.findAllTasksByInfoCurrentId(token, part);
        if (tasksByName == null) throw new Exception();
        if (tasksByInfo == null) throw new Exception();
        if (tasksByName.isEmpty() && tasksByInfo.isEmpty()) {
            terminalService.write("Task doesn't exist. Use \"task-list\" to show all tasks.");
            terminalService.separateLines();
            return null;
        }
        int counter = 1;
        @NotNull final Map<String, String> tasksByNameMap = new TreeMap<>();
        @NotNull final Map<String, String> tasksByInfoMap = new TreeMap<>();
        for (String taskName : tasksByName) {
            tasksByNameMap.put(String.valueOf(counter), taskName);
            counter++;
        }
        for (String taskName : tasksByInfo) {
            tasksByInfoMap.put(String.valueOf(counter), taskName);
            counter++;
        }
        @NotNull final Task task;
        while (true) {
            if (!tasksByNameMap.isEmpty()) terminalService.write("Found by name: ");
            tasksByNameMap.forEach((k, v) -> terminalService.write(k + ". " + v));
            if (!tasksByInfoMap.isEmpty()) terminalService.write("Found by description:");
            tasksByInfoMap.forEach((k, v) -> terminalService.write(k + ". " + v));
            terminalService.separateLines();
            terminalService.write("Select task.");
            terminalService.write("ENTER TASK NUMBER: ");
            @NotNull final String number = terminalService.enterIgnoreEmpty();
            if (!tasksByNameMap.containsKey(number) && !tasksByInfoMap.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of item doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            if (tasksByNameMap.containsKey(number)) {
                @NotNull final String taskName = tasksByNameMap.get(number);
                task = taskEndpoint.findByTaskNameCurrentId(token, taskName);
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                break;
            }
            if (tasksByInfoMap.containsKey(number)) {
                @NotNull final String taskName = tasksByNameMap.get(number);
                task = taskEndpoint.findByTaskNameCurrentId(token, taskName);
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                break;
            }
        }
        return task;
    }

}
