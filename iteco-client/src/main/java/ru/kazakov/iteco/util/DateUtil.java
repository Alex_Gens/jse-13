package ru.kazakov.iteco.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

public final class DateUtil {

    @Nullable
    public static Date parseDate(@Nullable final String date) throws Exception {
        if (date == null || date.isEmpty()) throw new Exception();
        try {
            @NotNull final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.uuuu");
            @NotNull final LocalDate localDate = LocalDate.parse(date, formatter);
            return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        } catch (DateTimeParseException e) {return null;}
    }

}
