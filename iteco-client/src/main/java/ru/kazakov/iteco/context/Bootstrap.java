package ru.kazakov.iteco.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.*;
import ru.kazakov.iteco.api.service.*;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.endpoint.*;
import ru.kazakov.iteco.service.TerminalService;
import ru.kazakov.iteco.util.Password;
import java.lang.Exception;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public final class Bootstrap implements ServiceLocator, CurrentState {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final ServiceLocator serviceLocator = this;

    @NotNull
    private final CurrentState currentState = this;

    @Getter
    @Setter
    @Nullable
    private String token = null;

    public void init() {
        try {
            registry();
            addUsers();
            start();
        } catch (InstantiationException |
                 InvocationTargetException |
                 NoSuchMethodException |
                 IllegalAccessException e) {e.getMessage();}
          catch (Exception e) {e.printStackTrace();}
    }

    private void start() {
        terminalService.write("*** WELCOME TO TASK MANAGER ***");
        @NotNull String command = "";
        try {
            while (true) {
                if (token == null || token.isEmpty()) {
                    terminalService.write("You are not authorized. Use \"user-login\" for authorization.");
                    terminalService.separateLines();
                }
                command = terminalService.enterIgnoreEmpty();
                command = command.trim().toLowerCase();
                execute(command);
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommands() {return new ArrayList<>(commands.values());}

    private void registry() throws IllegalAccessException, InstantiationException {
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("ru.kazakov.iteco").getSubTypesOf(AbstractCommand.class);
        classes.removeIf(clazz -> clazz.getName().contains("Abstract"));
        for (Class<?> clazz : classes
             ) {
            if (!AbstractCommand.class.isAssignableFrom(clazz)) continue;
            @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setServiceLocator(serviceLocator);
            command.setCurrentState(currentState);
            @NotNull final String name = command.getName();
            commands.put(name, command);
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        if (!commands.containsKey(command)) {
            terminalService.write("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            return;
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (token == null && abstractCommand.getRoleType() == RoleType.ADMINISTRATOR) {
            terminalService.write("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            return;
        }
        if ((token == null || token.isEmpty()) && abstractCommand.getRoleType() == RoleType.DEFAULT) {
            terminalService.write("[NO ACCESS]");
            return;
        }
        if ((token == null || token.isEmpty()) && abstractCommand.getRoleType() == RoleType.NONE) {
            abstractCommand.execute();
            return;
        }
        @Nullable final RoleType currentUserRoleType = sessionEndpoint.getSessionRoleType(token);
        if (currentUserRoleType == null) {
            terminalService.write("[NO ACCESS]");
            return;
        }
        if (currentUserRoleType !=  RoleType.ADMINISTRATOR && abstractCommand.getRoleType() == RoleType.ADMINISTRATOR) {
            terminalService.write("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            terminalService.separateLines();
            return;
        }
        abstractCommand.execute();
    }

    private void addUsers() throws Exception{
        @NotNull final User user = new User();
        user.setLogin("user");
        @NotNull final String password = Password.getHashedPassword("pass");
        user.setPassword(password);
        user.setRoleType(RoleType.DEFAULT);
        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPassword(password);
        admin.setRoleType(RoleType.ADMINISTRATOR);
        userEndpoint.persistUser(user);
        userEndpoint.persistUser(admin);
    }

}
